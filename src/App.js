import './App.css';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap'
import { useState, useEffect } from 'react'
import {UserProvider} from './UserContext'

// Components
import AppNavbar from './components/AppNavbar'
import ProductView from './components/ProductView'
// Pages
import Home from './pages/Home'
import Login from './pages/Login'
import Products from './pages/Products'
import Logout from './pages/Logout'
import Register from './pages/Register'
import Cart from './pages/Cart'
import Dashboard from './pages/Dashboard'
import EditView from './components/EditView'
import CreateProduct from './pages/CreateProduct'
import Error from './pages/Error'


function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null,
    });

    const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    fetch('https://evening-island-65608.herokuapp.com/users/details', {

      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }

    })
    .then(res => res.json())
    .then(data=> {

      if (typeof data._id !== "undefined") {

        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })

  },[])


  return (

  <UserProvider value={{user, setUser, unsetUser}}>

    <Router>

          <AppNavbar/>

        <Container>
          <Routes>

            {/*v5 routing: <Route exact path ="/" component={Home}>*/}

            <Route exact path= "/" element={<Home/>}/>
            <Route exact path= "/login" element={<Login/>}/>
            <Route exact path= "/products" element={<Products/>}/>
            <Route exact path= "/register" element={<Register/>}/>
            <Route exact path= "/cart" element={<Cart/>}/>
            <Route exact path= "/logout" element={<Logout/>}/>
            <Route exact path= "/dashboard" element={<Dashboard/>}/>
            <Route exact path = "/products/:productId" element={<ProductView/>}/>
            <Route exact path = "/products/get/:productId" element={<EditView/>}/>
            <Route exact path = "*" element={<Error/>}/>
            <Route exact path = "/create" element={<CreateProduct/>}/>
   
         </Routes>
        </Container>

      </Router>

  </UserProvider>

    )
}

export default App;
