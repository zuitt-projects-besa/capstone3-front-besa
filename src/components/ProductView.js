import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import{useParams, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

import '../App.css'


export default function ProductView(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const {productId} = useParams();
	const [quantity, setQuantity] = useState(1);

	const add = () =>{
		setQuantity(quantity + 1)
	}

	const minus = () =>{
		setQuantity(quantity - 1)

		if (quantity === 1) {
			setQuantity(1)
		}
	}

const addToCart = (productId) => {

		fetch('https://evening-island-65608.herokuapp.com/users/buy', {
			method: 'POST',
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				totalAmount: quantity,
				name : name,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log("show data")
			console.log(data)

			if (data === true){
				Swal.fire({
					title: `Added to cart`,
					icon: "success",
					text: `You have successfully added ${name} in your cart`,
					confirmButtonColor: 'green'
				})
				history("/cart")
			} else {
				Swal.fire({
					title: "Not for Admin Use",
					icon: "error",
					text: "Go Back",
					confirmButtonColor: 'red'
				})
			}
		})
	}


	useEffect(() => {

		fetch(`https://evening-island-65608.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)

		})

	}, [productId])

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card >
						<Card.Body className="containerDesign">
							<Card.Title className = "pt-3 title">{name}</Card.Title>

							<Card.Subtitle className = "pb-2 pt-3">Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle className = "py-2">Price</Card.Subtitle>
							<Card.Text className="title">Php {price}</Card.Text>
							
							
							{ user.id !== null ?
							<>
							<Card.Subtitle className = "py-2">Quantity: {quantity}</Card.Subtitle>
							<Card.Text >
							<Button variant="dark" onClick={minus}>-</Button>
							<Button className="mx-2" variant="dark" onClick={add}>+</Button>
							</Card.Text>
							<Button variant="success" onClick={()=>addToCart(productId)}>Add to Cart</Button>
							<Link className="btn btn-danger mx-2" to="/products">Go Back</Link>
							</>
							:
							<>
							<Link className="btn btn-dark mt-3 px-4 " to="/login">Login</Link>
							<Card.Text className="text-muted mt-2">Log in to use cart!</Card.Text>
							<Card.Text as={Link} to="/products" className="text-muted">Go back</Card.Text>
							</>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}