import { Row, Col, Card, Button,} from 'react-bootstrap'
import { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'

import '../App.css';


export default function ProductCards({productProp}) {

	const {name, description, price, _id} = productProp;

	return (

    <Card className="mb-3">
      <Card.Header>Featured Item</Card.Header>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Text className="priceText">Php {price}</Card.Text>
        <Button variant="dark" as={Link} to={`/products/${_id}`}>View Details</Button>
      </Card.Body>
      <Card.Footer className="text-muted">Available!</Card.Footer>
    </Card>

	)
}