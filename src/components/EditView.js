import {Container, Card, Button, Modal, Form} from 'react-bootstrap'
import {useState, useEffect, location} from 'react'
import {Link, useParams , useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import React from 'react'

import '../App.css'

export default function EditView({productProp}) {

	const {productId} = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setActive] = useState(false);
	const history = useNavigate()

	useEffect(() => {

		fetch(`https://evening-island-65608.herokuapp.com/products/get/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})
	}, [productId])

const updateDetails = (e) => {

	e.preventDefault();

	fetch(`https://evening-island-65608.herokuapp.com/products/updateProduct/${productId}`,{
		method: 'PUT',
		headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {})

	Swal.fire({
			title: `Edit Success`,
			icon: 'Success',
			text: `${name} has been edited successfully.`,
			confirmButtonColor: 'green'
		})

	history("/dashboard")
}


useEffect(() => {

	const formName = document.getElementById("nameForm").value
	const formDesc = document.getElementById("descForm").value
	const formPrice = document.getElementById("priceForm").value

		if (formName !== '' || formDesc !== '' || formPrice !== '') {
			setActive(true)
		} else {
			setActive(false)
		}

	},[name, description, price])

	return (

	<Container className="mt-5 p-3 containerDesign">
	
		<Form onSubmit={e => updateDetails(e)}>
		<h1 className="title">Edit Information</h1>

			<Form.Group controlId="nameForm">
				<Form.Label className="">Name:</Form.Label>
				<Form.Control
					type = "text"
					placeholder= {name}
					onChange = {e => setName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="descForm">
				<Form.Label className="">Description:</Form.Label>
				<Form.Control
					type = "text"
					placeholder= {description}
					onChange = {e => setDescription(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="priceForm">
				<Form.Label className="">Price:</Form.Label>
				<Form.Control
					type = "number"
					placeholder= {price}
					onChange = {e => setPrice(e.target.value)}
				/>
			</Form.Group>

			{ (isActive === true) ?

			<Button variant="warning" type="submit" className="px-4 mt-3">Edit</Button>

			:

			<Button variant="warning" type="submit" className="px-4 mt-3" disabled>Edit</Button>

			}

			<Button variant="danger" as={Link} to={`/dashboard`} type="submit" className="px-4 mx-2 mt-3">Go Back</Button>

		</Form>
	
	</Container>
	
	)
}