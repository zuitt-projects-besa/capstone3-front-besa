import {useState, useEffect} from 'react'
import EditView from '../components/EditView'




export default function Edit() {

	const [products, setProduct] = useState([]);

	useEffect(() => {
		fetch('https://evening-island-65608.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {

			setProduct(data.map(product => {
				return(
					<>
						<EditView key={product._id} productProp={product}/>
					</>
				);
			}));
		})
	}, [])

	return(
	<>
		<h1 className="py-3">Dashboard</h1>
		{products}
	</>
	)
}