import {useState, useEffect, useContext} from 'react'
import {Container, Button} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import DashboardView from '../components/DashboardView'
import '../App.css'

import UserContext from '../UserContext'


export default function Dashboard() {

	const {user} = useContext(UserContext)
	const [products, setProduct] = useState([]);

	useEffect(() => {
		fetch('https://evening-island-65608.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {

			setProduct(data.map(product => {
				console.log("isActive")
				console.log(product.isActive)
				return(
					<>
						<DashboardView key={product._id} productProp={product}/>
					</>
				);
			}));
		})
	}, [])

	return(
	
		(user.isAdmin === false) ?

		<Navigate to ="/"/>
		
		:
		<>
		<Container className="panel">
			<h1 className="mt-5 title">Admin Dashboard</h1>
			<Button as={Link} to="/create" variant="success">Create Product</Button>
		</Container>

		<Container>
		<h3 className="py-3 title" >Products</h3>
		{products}
		</Container>
	
		</>
	)
}