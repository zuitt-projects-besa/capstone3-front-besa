import {useState, useEffect} from 'react'
import ProductCards from '../components/ProductCards'
import '../App.css'


export default function Products() {

	const [products, setProduct] = useState([]);

	useEffect(() => {
		fetch('https://evening-island-65608.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {

			setProduct(data.map(product => {
				return(
					
					<ProductCards key={product._id} productProp={product}/>

				);
			}));
		})
	}, [])


	return(
	<>
		<h1 className="py-3 title">Available Items</h1>
		{products}
	</>
	)
}