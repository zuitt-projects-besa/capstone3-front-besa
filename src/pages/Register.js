import {Form, Button, Container} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import '../App.css'
export default function Register () {

	const {user} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const history = useNavigate()

function registerUser(e) {
	e.preventDefault();

	fetch('https://evening-island-65608.herokuapp.com/users/checkEmail', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email
		})
	})
	.then(res => res.json())
	.then(data => {
		if (data ===true){
			Swal.fire({
				title: "Duplicate Email Found",
				icon: "error",
				text: "Email already exist",
				confirmButtonColor: 'red'
			})
		} else {
			fetch('https://evening-island-65608.herokuapp.com/users/register', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {

			if (data === true) {

				setEmail('');
				setPassword('');

				Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "You can now login",
						confirmButtonColor: 'green'
					})
				history("/login")
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
						confirmButtonColor: 'red'
					})
				}
			})
		}
	})
}


	useEffect(() => {

		if ( (email !== '' && password !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password])


	return (

		(user.id !== null) ?
		<Navigate to='/products'/>
	
		:
		<Container className="containerDesign mt-5">
		<Form className="m-3" onSubmit={e => registerUser(e)}>
		<h1 className="py-3 title">Register</h1>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder= "Enter your email here"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Enter password here"
					value = {password}
					onChange = {e => setPassword (e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant="dark" type="submit" id="submitBtn" className="mt-3">
					Submit
				</Button>
				:
				<Button variant="dark" type="submit" id="submitBtn" className="mt-3" disabled>
					Submit
				</Button>

			}
			<Form.Group>
			<Form.Text as={Link} to="/login">Log in here</Form.Text>
			</Form.Group>

		</Form>

		</Container>
	)

}