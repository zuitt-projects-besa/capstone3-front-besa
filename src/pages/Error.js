import {Row, Col, Card, Nav, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

import '../App.css';

export default function Error() {
	return(
		<Row className=" m-5">
			<Col xs={12} md={12}>
				<Card className="p-3">
					<Card.Title className="errorMessage">
						<h1 className="">Page Not Found!</h1>
						<p className="text-muted">The page you are looking for does not exist</p>	
						<Button as={Link} to="/">Go Home</Button>
					</Card.Title>
				</Card>
			</Col>
		
		</Row>
	)
}