import {Form, Button, Container} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'

import '../App.css'

export default function CreateProduct () {

	const {user} = useContext(UserContext)
	const history = useNavigate()
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);


function createProduct(e) {
	e.preventDefault();

	fetch('https://evening-island-65608.herokuapp.com/products/createProduct/', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			name : name,
			description : description,
			price : price,
		})
	})
	.then(res => res.json())
	.then(data => {
		Swal.fire({
				title: `${name} successfully created`,
				icon: "success",
				text: "View product page",
				confirmButtonColor: 'green'
			})
		history("/dashboard")
	})
}

useEffect(() => {

	if ( (name !== '' && description !== '' && price !== '')) {
		setIsActive(true)
	} else {
		setIsActive(false)
	}

}, [name, description, price, setIsActive])


	return (

		(user.isAdmin === false) ?

		<Navigate to ="/"/>

		:

		<Container className="mt-5 containerDesign">
			<Form onSubmit={e => createProduct(e)}>
			<h1 className="py-3 title">Create Product</h1>

				<Form.Group controlId="productName">
					<Form.Label>Product Name</Form.Label>
					<Form.Control
						type = "text"
						placeholder= "Enter product name here"
						value = {name}
						onChange = {e => setName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="productDesc">
					<Form.Label>Product Description</Form.Label>
					<Form.Control
						type = "text"
						placeholder= "Enter product description here"
						value = {description}
						onChange = {e => setDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="productPrice">
					<Form.Label>Product Price</Form.Label>
					<Form.Control
						type = "number"
						placeholder= "Enter product price here"
						value = {price}
						onChange = {e => setPrice(e.target.value)}
						required
					/>
				</Form.Group>

				{ (isActive === true) ?	
				<Button variant="success" type="submit" id="submitBtn" className="my-2">Submit</Button>
				:
				<Button variant="success" type="submit" id="submitBtn" className="my-2" disabled>Submit</Button>
				}
				<Button variant="danger m-3" type="submit" id="submitBtn" className="my-2" as= {Link} to="/dashboard">Go Back</Button>
			
			</Form>
		</Container>
	);
}